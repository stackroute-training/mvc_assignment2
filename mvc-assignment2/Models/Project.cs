﻿namespace mvc_assignment2.Models
{
    public class Project
    {
        public int projId { get; set; }
        public string projName { get; set; }
        public string projStatus { get; set; }
        public int projCost { get; set; }
    }
}
