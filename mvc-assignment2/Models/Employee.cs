﻿namespace mvc_assignment2.Models
{
    public class Employee
    {
        public int empId { get; set; }
        public string name { get; set; }
        public string Joindate { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string city { get; set; }
    }
}
