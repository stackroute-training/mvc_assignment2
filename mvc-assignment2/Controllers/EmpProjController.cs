﻿using Microsoft.AspNetCore.Mvc;
using mvc_assignment2.EmployeeProjectViewModel;
using mvc_assignment2.Models;

namespace mvc_assignment2.Controllers
{
    public class EmpProjController : Controller
    {
        public IActionResult Index()
        {
            Employee emp = new() { empId = 1, name = "poco", Joindate = "9-2-22", email = "poco@gmail.com", mobile = "1234567", city = "delhi" };
            Project project = new() { projId = 1, projName = "Project1", projCost = 20000, projStatus = "Completed" };
            EmployeeProjectViewModel.EmployeeProjectViewModel empProj = new() { employee=emp, project=project};
            return View(empProj);
        }
    }
}
