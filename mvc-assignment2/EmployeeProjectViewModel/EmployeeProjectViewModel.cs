﻿using mvc_assignment2.Models;
namespace mvc_assignment2.EmployeeProjectViewModel
{
    public class EmployeeProjectViewModel
    {
        public Employee employee { get; set; }
        public Project project { get; set; }
    }
}
